trigger UpdateRelatedOpportunity on Discount_Commission_Contract__c (before delete, after insert, after update) {
	// insert
	if (trigger.isInsert) {
		// apply to opportunity
		ApplyDCContractClass.ApplyDCCToOpportunity(trigger.new);		
	}
	
	// update
	if (trigger.isUpdate) {
		Integer index = 0;
		Map<Id, Discount_Commission_Contract__c> mapDCc = new Map<Id, Discount_Commission_Contract__c>(); 
		for (Discount_Commission_Contract__c dcc : trigger.new) {
			// affiliate or startdate or enddate has changed
			if (dcc.Affiliate_Partner__c != trigger.old[index].Affiliate_Partner__c || dcc.Start_Date__c != trigger.old[index].Start_Date__c || 
				dcc.End_Date__c != trigger.old[index].End_Date__c || dcc.Type__c != trigger.old[index].Type__c) {
				mapDCc.put(dcc.Id, dcc);
			}
			index++;
		}
		
		if (!mapDCc.isEmpty()) {
			// remove opportunity dc contract lookup
			ApplyDCContractClass.RemoveOpportunityDCC(mapDCc.keySet());
			
			// apply to opportunity
			ApplyDCContractClass.ApplyDCCToOpportunity(mapDCc.Values());
		}
	}
	
	// delete
	if (trigger.isDelete) {
		// remove opportunity dc contract lookup
		ApplyDCContractClass.RemoveOpportunityDCC(trigger.oldMap.keySet());
	}
}