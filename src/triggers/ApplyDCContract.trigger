trigger ApplyDCContract on Opportunity (before insert, before update, after update) {
	Map<Id, Id> mapOppDCc = new Map<Id, Id>();
	
    Set<Id> actIds = new Set<Id>();
    if (trigger.isInsert) {
        if (trigger.new.size() == 1) {
            List<Discount_Commission_Contract__c> dcContract = [Select Id From Discount_Commission_Contract__c Where Affiliate_Partner__c = :trigger.new[0].AccountId and Start_Date__c <= :trigger.new[0].CloseDate and End_Date__c >= :trigger.new[0].CloseDate Order By Type__c desc limit 1];
            if (!dcContract.isEmpty()) {
                trigger.new[0].Discount_Commission_Contract__c = dcContract[0].Id;
            }
        } else {           
            for (Opportunity op : trigger.new) {
                actIds.add(op.AccountId);
            }            
            // update Opportunity DC contract
           	ApplyDCContractClass.UpdateOpportunityDCContract(trigger.new, actIds);            
        }
    } else { // update    	    	               
        Integer index = 0;
    	if (trigger.isBefore) { // before
    		List<Opportunity> oppChangeList = new List<Opportunity>();
    		for (Opportunity opp : trigger.new) {
    			// account changed
	            if (opp.AccountId != trigger.old[index].AccountId) {
	            	// with old account change to no account
	                if (trigger.old[index].AccountId != null && opp.AccountId == null) {	                    
	                    opp.Discount_Commission_Contract__c = null;
	                } else { // no old account changed to with account or old account changed to new account                
	                    oppChangeList.add(opp);
	                    actIds.add(opp.AccountId);
	                } 
	            } else {
	                // close date changed
	                if (opp.CloseDate != trigger.old[index].CloseDate) {
	                    oppChangeList.add(opp);
	                    actIds.add(opp.AccountId);
	                } 
	            }
	            index++;
    		}
    		
    		// Changed Opps
	        if (!oppChangeList.isEmpty()) {
	            if (oppChangeList.size() == 1) { // one record            	
	                List<Discount_Commission_Contract__c> dcContract = [Select Id From Discount_Commission_Contract__c Where Affiliate_Partner__c = :oppChangeList[0].AccountId and Start_Date__c <= :oppChangeList[0].CloseDate and End_Date__c >= :oppChangeList[0].CloseDate Order By Type__c desc limit 1];
	                if (!dcContract.isEmpty()) {
	                    oppChangeList[0].Discount_Commission_Contract__c = dcContract[0].Id;	                                                                            		        	                                     
	                } else {
	                	oppChangeList[0].Discount_Commission_Contract__c = null;	                	                   
	                }                
	            } else { // more records
	            	// update Opportunity DC contract
	            	ApplyDCContractClass.UpdateOpportunityDCContract(oppChangeList, actIds);	            	
	            }
	        } 
    	} else { // after
    		List<Opportunity> oppChangeList = new List<Opportunity>();
    		Set<Id> oppRemoveSet = new Set<Id>();
    		Map<Id, Id> newMapOppDCc = new Map<Id, Id>(); 
    		for (Opportunity opp : trigger.new) { 
    			// dccontract has changed
        		if (opp.Discount_Commission_Contract__c != trigger.old[index].Discount_Commission_Contract__c) {
        			// with old dccontract to no dccontract
	        		if (trigger.old[index].Discount_Commission_Contract__c != null && opp.Discount_Commission_Contract__c == null) {
	        			oppRemoveSet.add(opp.Id);
	        		} else {	        			
	        			mapOppDCc.put(opp.Id, opp.Discount_Commission_Contract__c);                   
	        		}
        		} else {
        			// account changed
		            if (opp.AccountId != trigger.old[index].AccountId) {
		                // with old account change to no account
		                if (trigger.old[index].AccountId != null && opp.AccountId == null) {
		                    oppRemoveSet.add(opp.Id);		                    
		                } else { // no old account changed to with account or old account changed to new account                
		                    oppChangeList.add(opp);
		                    newMapOppDCc.put(opp.Id, opp.Discount_Commission_Contract__c);
		                } 
		                // 
		            } else {
		                // close date changed
		                if (opp.CloseDate != trigger.old[index].CloseDate) {
		                    oppChangeList.add(opp);
		                    newMapOppDCc.put(opp.Id, opp.Discount_Commission_Contract__c);
		                } 
		            }
        		}
        		index++;
    		}
    		
    		// Removed Opps (remove DC on opplineitems)
	        if (!oppRemoveSet.isEmpty()) {            
	            ApplyDCContractClass.RemoveOppItemsDC(oppRemoveSet);
	        }
	        
	        // Update Opps (update DC on opplineitems)
	        if (!mapOppDCc.isEmpty()) {
	        	ApplyDCContractClass.UpdateOppItemsDC(mapOppDCc);        	       
	        }
	        
	        // Changed Opps
	        if (!oppChangeList.isEmpty()) {
	            if (oppChangeList.size() == 1) { // one record            	
	                List<Discount_Commission_Contract__c> dcContract = [Select Id From Discount_Commission_Contract__c Where Affiliate_Partner__c = :oppChangeList[0].AccountId and Start_Date__c <= :oppChangeList[0].CloseDate and End_Date__c >= :oppChangeList[0].CloseDate Order By Type__c desc limit 1];
	                if (!dcContract.isEmpty()) {	                    
	                    // update opp items dc	                    
	                    ApplyDCContractClass.UpdateOppItemsDC(new Map<Id, Id>{oppChangeList[0].Id => dcContract[0].Id});                                                        		        	                                     
	                } else {	                	
	                	// no DC Contract found, remove DC on opplineitems
	                	ApplyDCContractClass.RemoveOppItemsDC(new Set<Id>{oppChangeList[0].Id});                    
	                }                
	            } else { // more records	            		            	
	            	// update opp items dc
	                if (!newMapOppDCc.isEmpty()) {
	                	ApplyDCContractClass.UpdateOppItemsDC(newMapOppDCc);
	                }
	            }
	        }  
	        
    	}    	       
    }
}

/*
trigger ApplyDCContract on Opportunity (before insert, before update) {
	Map<Id, Id> mapOppDCc = new Map<Id, Id>();
    Set<Id> actIds = new Set<Id>();
    if (trigger.isInsert) {
        if (trigger.new.size() == 1) {
            List<Discount_Commission_Contract__c> dcContract = [Select Id From Discount_Commission_Contract__c Where Affiliate_Partner__c = :trigger.new[0].AccountId and Start_Date__c <= :trigger.new[0].CloseDate and End_Date__c >= :trigger.new[0].CloseDate Order By Type__c desc limit 1];
            if (!dcContract.isEmpty()) {
                trigger.new[0].Discount_Commission_Contract__c = dcContract[0].Id;
            }
        } else {           
            for (Opportunity op : trigger.new) {
                actIds.add(op.AccountId);
            }            
            // update Opportunity DC contract
           	mapOppDCc = ApplyDCContractClass.UpdateOpportunityDCContract(trigger.new, actIds);            
        }
    } else { // update
        List<Opportunity> oppChangeList = new List<Opportunity>();
        List<Opportunity> oppRemoveList = new List<Opportunity>();
        List<Opportunity> oppUpdateList = new List<Opportunity>();        
        Integer index = 0;
        for (Opportunity opp : trigger.new) {    
        	// dccontract has changed
        	if (opp.Discount_Commission_Contract__c != trigger.old[index].Discount_Commission_Contract__c) {
        		// with old dccontract to no dccontract
        		if (trigger.old[index].Discount_Commission_Contract__c != null && opp.Discount_Commission_Contract__c == null) {
        			oppRemoveList.add(opp);
        		} else {
        			oppUpdateList.add(opp);	 
        			mapOppDCc.put(opp.Id, opp.Discount_Commission_Contract__c);                   
        		}
        	} else {      
	            // account changed
	            if (opp.AccountId != trigger.old[index].AccountId) {
	                // with old account change to no account
	                if (trigger.old[index].AccountId != null && opp.AccountId == null) {
	                    oppRemoveList.add(opp);
	                    opp.Discount_Commission_Contract__c = null;
	                } else { // no old account changed to with account or old account changed to new account                
	                    oppChangeList.add(opp);
	                    actIds.add(opp.AccountId);
	                } 
	                // 
	            } else {
	                // close date changed
	                if (opp.CloseDate != trigger.old[index].CloseDate) {
	                    oppChangeList.add(opp);
	                    actIds.add(opp.AccountId);
	                } 
	            }
        	}
            index++;
        }
                
        // Removed Opps (remove DC on opplineitems)
        if (!oppRemoveList.isEmpty()) {            
            ApplyDCContractClass.RemoveOppItemsDC(oppRemoveList);
        }
        
        // Update Opps (update DC on opplineitems)
        if (!oppUpdateList.isEmpty()) {
        	ApplyDCContractClass.UpdateOppItemsDC(mapOppDCc);        	       
        }
        
        // Changed Opps
        if (!oppChangeList.isEmpty()) {
            if (oppChangeList.size() == 1) { // one record            	
                List<Discount_Commission_Contract__c> dcContract = [Select Id From Discount_Commission_Contract__c Where Affiliate_Partner__c = :oppChangeList[0].AccountId and Start_Date__c <= :oppChangeList[0].CloseDate and End_Date__c >= :oppChangeList[0].CloseDate Order By Type__c desc limit 1];
                if (!dcContract.isEmpty()) {
                    oppChangeList[0].Discount_Commission_Contract__c = dcContract[0].Id;
                    // update opp items dc
                    mapOppDCc.put(oppChangeList[0].Id, dcContract[0].Id);
                    ApplyDCContractClass.UpdateOppItemsDC(mapOppDCc);                                                        		        	                                     
                } else {
                	oppChangeList[0].Discount_Commission_Contract__c = null;
                	// no DC Contract found, remove DC on opplineitems
                	ApplyDCContractClass.RemoveOppItemsDC(oppChangeList);                    
                }                
            } else { // more records
            	// update Opportunity DC contract
            	mapOppDCc = ApplyDCContractClass.UpdateOpportunityDCContract(oppChangeList, actIds);
            	// update opp items dc
                if (!mapOppDCc.isEmpty()) {
                	ApplyDCContractClass.UpdateOppItemsDC(mapOppDCc);
                }
            }
        }    
    }
}
*/