trigger ApplyDiscountCommision on OpportunityLineItem (before insert) {
	if (trigger.isInsert) {				
		// get all opportunity id and pricebookentry id
		Set<Id> oppIds = new Set<Id>();	
		Set<Id> pbeIds = new Set<Id>();
		for (OpportunityLineItem i : trigger.new) {
	        oppIds.add(i.OpportunityId); 
	        pbeIds.add(i.PricebookEntryId);       
	    }
	    
	    // map opporturnity and dc contract 
	    Map<Id, Id> mapOppDCc = new Map<Id, Id>();
	    for (Opportunity op : [Select Id, Discount_Commission_Contract__c From Opportunity Where Id in :oppIds and Discount_Commission_Contract__c != null]) {
	    	mapOppDCc.put(op.Id, op.Discount_Commission_Contract__c);
	    }
	    System.debug('mapOppDCc : ' + mapOppDCc);
	    
	    if (!mapOppDCc.isEmpty()) {
			// get dc
        	Map<Id, Map<String, Discount_Commission__c>> mapDC = new Map<Id, Map<String, Discount_Commission__c>>();
        	for (Discount_Commission__c d : [Select Id, Discount_Commission_Contract__c, Product__c, PriceBookId__c, Affiliate_Commision__c, Customer_Discount__c From Discount_Commission__c where Discount_Commission_Contract__c in :mapOppDCc.values()]) {
	            if (mapDC.get(d.Discount_Commission_Contract__c) == null) {
	                mapDC.put(d.Discount_Commission_Contract__c, new Map<String, Discount_Commission__c>());
	            }
            	mapDC.get(d.Discount_Commission_Contract__c).put(String.valueOf(d.Product__c)+d.PriceBookId__c, d);
        	}    
        	System.debug('mapDC: ' + mapDC);
        	
        	// process here
        	if (!mapDC.isEmpty()) {
        		Map<Id, PricebookEntry> mapPriceBookEntry = new Map<Id, PricebookEntry>([Select Id, Product2Id, Pricebook2Id From PricebookEntry where Id in :pbeIds]); 
            	for (OpportunityLineItem i : trigger.new) {
                	if (mapOppDCc.containsKey(i.OpportunityId)) {
                    	Id dcConId = mapOppDCc.get(i.OpportunityId);
                    	if (mapDC.containsKey(dcConId)) {
                        	PricebookEntry pbe = mapPriceBookEntry.get(i.PricebookEntryId);
                        	Map<String, Discount_Commission__c> mapDCValues = mapDC.get(dcConId);
                        	if (mapDCValues.containsKey(String.valueOf(pbe.Product2Id)+String.valueOf(pbe.Pricebook2Id))) {
                            	Discount_Commission__c dc = mapDCValues.get(String.valueOf(pbe.Product2Id)+String.valueOf(pbe.Pricebook2Id));                            
                            	if (i.Discount == null) { // discount to 0 if null
                                	i.Discount = 0;
                            	}
                                                                                                                
                            	String descr = '';
	                            // affiliate commision
	                            if (dc.Affiliate_Commision__c != null && dc.Affiliate_Commision__c != 0) {
	                                i.Discount += dc.Affiliate_Commision__c;
	                                i.Affiliate_Commision__c = dc.Affiliate_Commision__c;
	                                descr = 'Affiliate Commision = ' + String.valueOf(dc.Affiliate_Commision__c);                                
	                            }                            
	                            // customer discount
	                            if (dc.Customer_Discount__c != null && dc.Customer_Discount__c != 0) {
	                                i.Discount += dc.Customer_Discount__c;
	                                i.Customer_Discount__c = dc.Customer_Discount__c;
	                                if (descr != '') descr+= '; '; 
	                                descr += 'Customer Discount = ' + String.valueOf(dc.Customer_Discount__c);
	                            }           
	                            
	                            // check if discount > 100
			                    if (i.Discount > 100) {
			                    	i.Discount = 100;
			                    } 
	                                             
                            	i.Description = descr;
                            	i.Discount_Commission__c = dc.Id; 
                        	}
                    	}
                	}
            	}
        	}							    		    			
	    }

	} else { // update
		/*
		Integer index = 0;
		List<OpportunityLineItem> newOppDcs = new List<OpportunityLineItem>();			
		Map<Id, Id> mapOppItemDc = new Map<Id, Id>();
		for (Integer i=0; i<trigger.new.size(); i++) {
			// dc has changed
			if (trigger.new[i].Discount_Commission__c != trigger.old[i].Discount_Commission__c) {
				// previously with value to null dc
				if (trigger.old[i].Discount_Commission__c != null && trigger.new[i].Discount_Commission__c == null) {
					// reset
					trigger.new[i].Description = '';
					if (trigger.old[i].Affiliate_Commision__c != null)
						trigger.new[i].Discount -= trigger.old[i].Affiliate_Commision__c;
					if (trigger.old[i].Customer_Discount__c != null)
						trigger.new[i].Discount -= trigger.old[i].Customer_Discount__c;	  	
					trigger.new[i].Affiliate_Commision__c = 0;
					trigger.new[i].Customer_Discount__c = 0;												
				} else {
					newOppDcs.add(trigger.new[i]);
					mapOppItemDc.put(trigger.new[i].Id, trigger.new[i].Discount_Commission__c);
				}
			} 
		}				
		
		// changed opportunitylineitem's dc
		if (!newOppDcs.isEmpty()) {
			Map<Id, Discount_Commission__c> mapDc = new Map<Id, Discount_Commission__c>([Select Id, Affiliate_Commision__c, Customer_Discount__c From Discount_Commission__c where Id in : mapOppItemDc.values()]);
			for (OpportunityLineItem i : newOppDcs) {				
				if (mapDc.containsKey(i.Discount_Commission__c)) {
					Discount_Commission__c dc = mapDc.get(i.Discount_Commission__c);
					if (i.Discount == null) { // discount to 0 if null
                    	i.Discount = 0;
                	}
                                                                                                    
                	String descr = '';
                    // affiliate commision
                    if (dc.Affiliate_Commision__c != null && dc.Affiliate_Commision__c != 0) {
                        i.Discount += dc.Affiliate_Commision__c;                        
                        descr = 'Affiliate Commision = ' + String.valueOf(dc.Affiliate_Commision__c);                                
                    }                            
                    // customer discount
                    if (dc.Customer_Discount__c != null && dc.Customer_Discount__c != 0) {
                        i.Discount += dc.Customer_Discount__c;                        
                        if (descr != '') descr+= '; '; 
                        descr += 'Customer Discount = ' + String.valueOf(dc.Customer_Discount__c);
                    }                            
                	i.Description = descr;                	
                	i.Affiliate_Commision__c = dc.Affiliate_Commision__c;
                	i.Customer_Discount__c = dc.Customer_Discount__c;
				}
			}
		}
		*/
	}		


	/*
    Set<Id> oppIds = new Set<Id>();
    Set<Id> pbeIds = new Set<Id>();
    for (OpportunityLineItem i : trigger.new) {
        oppIds.add(i.OpportunityId);
        pbeIds.add(i.PricebookEntryId);
    }
    
    // get opportunity
    Map<Id, Opportunity> mapOpp = new Map<Id, Opportunity>();
    Set<Id> actIds = new Set<Id>();
    for(Opportunity op : [Select Id, AccountId, CloseDate From Opportunity Where Id in :oppIds]) {
        mapOpp.put(op.Id, op);
        actIds.add(op.AccountId);
    }
    
    // get dc contract per account
    Map<Id, List<Discount_Commission_Contract__c>> mapAcctDC = new Map<Id, List<Discount_Commission_Contract__c>>();
    for (Discount_Commission_Contract__c dc : [Select Id, Affiliate_Partner__c, Start_Date__c, End_Date__c, Type__c From Discount_Commission_Contract__c where Affiliate_Partner__c in :actIds Order by Type__c desc, Start_Date__c]) {
        if (mapAcctDC.get(dc.Affiliate_Partner__c) == null) {
            mapAcctDC.put(dc.Affiliate_Partner__c, new List<Discount_Commission_Contract__c>());
        }
        mapAcctDC.get(dc.Affiliate_Partner__c).add(dc);
    }    
    System.debug('mapAcctDC : ' + mapAcctDC);
    
    if (!mapAcctDC.isEmpty()) {
        Map<Id, Id> mapOppDC = new Map<Id, Id>();
        Set<Id> dcIds = new Set<Id>();
        // loop opportunity
        Set<Id> oppSet = mapOpp.keySet();
        for (Id oId : oppSet) {
            Opportunity op = mapOpp.get(oId);
            List<Discount_Commission_Contract__c> dcList = mapAcctDC.get(op.AccountId);                            
            for (Discount_Commission_Contract__c dc : dcList) {
                if (op.CloseDate >= dc.Start_Date__c && op.CloseDate <= dc.End_Date__c) {
                    mapOppDC.put(oId, dc.Id);
                    if (dc.Type__c == 'Campaign') {
                        dcIds.add(dc.Id);
                        break;
                    }
                }
            } 
        }
        
        System.debug('mapOppDC : ' + mapOppDC);
        System.debug('dcIds: ' + dcIds);
        
        // get dc
        Map<Id, Map<String, Discount_Commission__c>> mapDC = new Map<Id, Map<String, Discount_Commission__c>>();
        for (Discount_Commission__c d : [Select Discount_Commission_Contract__c, Product__c, PriceBookId__c, Affiliate_Commision__c, Customer_Discount__c From Discount_Commission__c where Discount_Commission_Contract__c in :dcIds]) {
            if (mapDC.get(d.Discount_Commission_Contract__c) == null) {
                mapDC.put(d.Discount_Commission_Contract__c, new Map<String, Discount_Commission__c>());
            }
            mapDC.get(d.Discount_Commission_Contract__c).put(String.valueOf(d.Product__c)+d.PriceBookId__c, d);
        }    
        System.debug('mapDC: ' + mapDC);
    
        // process here
        if (!mapDC.isEmpty()) {       
            Map<Id, PricebookEntry> mapPriceBookEntry = new Map<Id, PricebookEntry>([Select Id, Product2Id, Pricebook2Id From PricebookEntry where Id in :pbeIds]); 
            for (OpportunityLineItem i : trigger.new) {
                if (mapOppDC.containsKey(i.OpportunityId)) {
                    Id dcId = mapOppDC.get(i.OpportunityId);
                    if (mapDC.containsKey(dcId)) {
                        PricebookEntry pbe = mapPriceBookEntry.get(i.PricebookEntryId);
                        Map<String, Discount_Commission__c> mapDCValue = mapDC.get(dcId);
                        if (mapDCValue.containsKey(String.valueOf(pbe.Product2Id)+String.valueOf(pbe.Pricebook2Id))) {
                            Discount_Commission__c dc = mapDCValue.get(String.valueOf(pbe.Product2Id)+String.valueOf(pbe.Pricebook2Id));                            
                            if (i.Discount == null) {
                                i.Discount = 0;
                            }
                                                                                                                
                            String descr;
                            // affiliate commision
                            if (dc.Affiliate_Commision__c != null && dc.Affiliate_Commision__c != 0) {
                                i.Discount += dc.Affiliate_Commision__c;
                                descr = 'Affiliate Commision = ' + String.valueOf(dc.Affiliate_Commision__c) + '\n';                                
                            }                            
                            // customer discount
                            if (dc.Customer_Discount__c != null && dc.Customer_Discount__c != 0) {
                                i.Discount += dc.Customer_Discount__c;
                                descr += 'Customer Discount = ' + String.valueOf(dc.Customer_Discount__c);
                            }                            
                            i.Description = descr;
                        }
                    }
                }
            }
        }
    }
    */
}