trigger applyPriceBookName on Discount_Commission__c (before insert, before update) {
    Set<String> pbIds = new Set<String>();
    for (Discount_Commission__c d : trigger.new) {
        if (d.PriceBookId__c != null && d.PriceBookId__c != '') {
            pbIds.add(d.PriceBookId__c);
        }
    }
    
    if (pbIds.size() > 0) {
        Map<Id, Pricebook2> mapPricebook = new Map<Id, Pricebook2>([Select Id, Name From Pricebook2 where Id in :pbIds]);
        if (!mapPricebook.isEmpty()) {
             for (Discount_Commission__c d : trigger.new) {
                if (d.PriceBookId__c != null && d.PriceBookId__c != '') {         
                    if (mapPricebook.containsKey(d.PriceBookId__c)) {
                        d.Price_Book_Name__c = mapPricebook.get(d.PriceBookId__c).Name;
                    }
                }
             }
        }
    }    
}